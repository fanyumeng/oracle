# 实验5：包，过程，函数的用法
姓名：范雨蒙
学号：202010414303
班级：软件工程3班

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
  Oracle递归查询的语句格式是：
```
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 实验注意事项，完成时间： 2023-05-16日前上交

- 请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的Oracle项目中的test5目录中。
- 上交后，通过这个地址应该可以打开你的源码：[test/test5 · main · 范 雨蒙 / oracle · GitLab](https://gitlab.com/fanyumeng/oracle/-/tree/main/test/test5)
- 实验分析及结果文档说明书用Markdown格式编写。

## 脚本代码参考

### 创建一个包：包名为MyPack
```
create or replace PACKAGE MyPack IS
FUNCTION SaleAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
END MyPack;
```
```
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
```
```
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

## 测试
函数Get_SalaryAmount()测试方法：
```
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```
输出：
```
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                           4427
           20 Marketing                              19054
           30 Purchasing                             25062
           40 Human Resources                       6527
           50 Shipping                              157615
           60 IT                                    28935
           70 Public Relations                         10027
           80 Sales                                305418
           90 Executive                              58081
          100 Finance                               51770
          110 Accounting                             20362

DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
          120 Treasury                                   
          130 Corporate Tax                              
          140 Control And Credit                           
          150 Shareholder Services                         
          160 Benefits                                    
          170 Manufacturing                               
          180 Construction                                
          190 Contracting                                 
          200 Operations                                  
          210 IT Support                                 
          220 NOC                                       

DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
          230 IT Helpdesk                                
          240 Government Sales                           
          250 Retail Sales                                 
          260 Recruiting                                   
          270 Payroll                                     

已选择 27 行。
```
过程Get_Employees()测试代码：
```
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/
```
输出：
```
101 Neena
    108 Nancy
        109 Daniel
        110 John
        111 Ismael
        112 Jose Manuel
        113 Luis
    200 Jennifer
    203 Susan
    204 Hermann
    205 Shelley
        206 William

PL/SQL 过程已成功完成。
```

## 实验总结
此次实验，是基于实验四完成的，都是在Oracle SQL Developer中完成的，语句和sql语句很像，由于老师给过示例代码，因此实验完成起来较为轻松。通过此次实验，我学会了如何创建包，如何创建存储过程以及如何进行表格的查询等，收获颇多。