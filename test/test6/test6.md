# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](/zwdcdu/oracle/-/blob/main/./README.md)
  
#### 姓名：范雨蒙
#### 学号：202010414303
#### 班级：20软件工程3班
  
- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
 - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
 - 设计权限及用户分配方案。至少两个用户。
 - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
 - 设计一套数据库的备份方案。
  
## 期末考核要求
  
- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
 - test6.md主文件。
 - 数据库创建和维护用的脚本文件*.sql。
 - [test6_design.docx](/zwdcdu/oracle/-/blob/main/%E4%B8%8A%E4%BA%A4%E5%AE%9E%E9%AA%8C/test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前
  
## 评分标准
  
| 评分项     | 评分标准                             | 满分 |
| ---------- | ------------------------------------ | ---- |
| 文档整体   | 文档内容详实、规范，美观大方         | 10   |
| 表设计     | 表设计及表空间设计合理，样例数据合理 | 20   |
| 用户管理   | 权限及用户分配方案设计正确           | 20   |
| PL/SQL设计 | 存储过程和函数设计正确               | 30   |
| 备份方案   | 备份方案设计正确                     | 20   |
  
  
### 总体方案：
#### 一、表空间设计方案：

- 主表空间（ProductSales_Data）：用于存储主要的表和索引数据。
- 索引表空间（ProductSales_Index）：用于存储索引数据，提高查询性能。
- 表设计方案：
- a. 商品表（Product）：存储商品信息，包括商品ID、名称、价格字段。
- b. 客户表（users）：存储客户信息，包括用户ID、用户名、密码、邮箱、电话号码字段。
- c. 销售订单表（Orders）：存储销售订单信息，包括订单ID、订单日期、客户ID字段。
- d. 销售明细表（order_details）：存储销售订单的商品明细，包括订单ID、商品ID、数量字段。

#### 二、权限与用户分配方案：
  
- 创建两个用户：PRODUCTSALES_USER和PRODUCTSALES_SYSTEM_ADMIN。
- PRODUCTSALES_USER用户用于应用程序访问数据库，具有对商品表、客户表、销售订单表和销售订单详情表的增删改查权限。
- PRODUCTSALES_SYSTEM_ADMIN用户用于管理数据库，具有对表空间、用户、存储过程和备份操作的权限。

#### 三、存储过程与函数设计：

- 创建一个程序包（SALES_PACKAGE）来封装存储过程和函数。
- 存储过程：
- CREATE_SALES_ORDER: 用于创建新的销售订单。接收客户ID、商品ID和数量作为参数，并将订单和订单详情插入到相应的表中，完成订单的创建过程。
- CANCEL_SALES_ORDER: 用于取消已有的销售订单。接收订单ID作为参数，并删除对应的订单和订单详情记录，从数据库中取消该订单。
- CANCEL_ORDERS_BY_CUSTOMER: 用于批量取消某个客户的订单。接收客户ID作为参数，并删除该客户的所有订单和订单详情记录，实现批量取消订单的功能。
- 函数：
- CALCULATE_ORDER_TOTAL:用于根据订单ID计算订单的总金额。接收订单ID作为参数，在数据库中检索订单详情，并计算订单中商品的总金额，然后返回计算得到的总金额值。
- 以及包括将所有的查询语句封装成函数。

#### 四、数据库备份方案：

- 定期执行完整备份：使用Oracle的RMAN工具，定期执行完整备份，将数据库的所有数据备份。

### 具体步骤

#### 第1步：表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。

```sql
-- 创建主表空间
CREATE TABLESPACE sales_data DATAFILE 'sales_data.dbf' SIZE 10M AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED;

-- 创建索引表空间
CREATE TABLESPACE sales_index DATAFILE 'sales_index.dbf' SIZE 10M AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED;

-- 创建商品表
CREATE TABLE products (
id NUMBER(10) PRIMARY KEY,
name VARCHAR2(100),
price NUMBER(10, 2),
description VARCHAR2(255),
image VARCHAR2(255)
);

-- 创建客户表
CREATE TABLE users (
id NUMBER(10) PRIMARY KEY,
username VARCHAR2(50) UNIQUE,
password VARCHAR2(50),
email VARCHAR2(100),
phone VARCHAR2(20)
);

-- 创建销售订单表
CREATE TABLE orders (
id NUMBER(10) PRIMARY KEY,
user_id NUMBER(10),
total_price NUMBER(10, 2),
create_time TIMESTAMP
);

-- 创建销售订单详情表
CREATE TABLE order_details (
id NUMBER(10) PRIMARY KEY,
order_id NUMBER(10),
product_id NUMBER(10),
quantity NUMBER(10),
unit_price NUMBER(10, 2),
subtotal NUMBER(10, 2)
);

-- 创建序列
CREATE SEQUENCE sales_seq START WITH 1 INCREMENT BY 1;

-- 为users表生成20000条数据的PL/SQL代码：
DECLARE
v_username VARCHAR2(50);
v_password VARCHAR2(50);
v_email VARCHAR2(100);
v_phone VARCHAR2(20);
BEGIN
FOR i IN 1..20000 LOOP
v_username := 'user' || i;
v_password := '123456';
v_email := v_username || '@gmail.com';
v_phone := '9' || LPAD(TRUNC(DBMS_RANDOM.VALUE(0, 999999999)), 9, 0);
  
INSERT INTO sales_datausers(id, username, password, email, phone)
VALUES(sales_seq.nextval, v_username, v_password, v_email, v_phone);
  
IF MOD(i, 1000) = 0 THEN
COMMIT;
END IF;
END LOOP;
COMMIT;
END;
/
-- 为products表生成20000条数据的PL/SQL代码：
DECLARE
v_name VARCHAR2(100);
v_price NUMBER(10, 2);
v_description VARCHAR2(255);
v_image VARCHAR2(255);
BEGIN
FOR i IN 1..20000 LOOP
v_name := 'product' || i;
    v_price := TRUNC(DBMS_RANDOM.VALUE(10, 10000)) / 100;
v_description := 'This is product' || i;
v_image := 'https://example.com/products/' || i || '.jpg';
  
INSERT INTO products(id, name, price, description, image)
VALUES(sales_seq.nextval, v_name, v_price, v_description, v_image);
  
IF MOD(i, 1000) = 0 THEN
COMMIT;
END IF;
END LOOP;
  
COMMIT;
END;
/
-- 为orders表生成20000条数据的PL/SQL代码：
DECLARE
v_user_id NUMBER(10);
v_total_price NUMBER(10, 2);
v_create_time TIMESTAMP;
BEGIN
FOR i IN 1..20000 LOOP
    v_user_id := TRUNC(DBMS_RANDOM.VALUE(1, 20000));
    v_total_price := TRUNC(DBMS_RANDOM.VALUE(10, 10000)) / 100;
    v_create_time := SYSTIMESTAMP - TRUNC(DBMS_RANDOM.VALUE(0, 180));
  
INSERT INTO orders(id, user_id, total_price, create_time)
VALUES(sales_seq.nextval, v_user_id, v_total_price, v_create_time);
  
IF MOD(i, 1000) = 0 THEN
COMMIT;
END IF;
END LOOP;
  
COMMIT;
END;
/
-- 为order_details表生成20000条数据的PL/SQL代码：
DECLARE
v_order_id NUMBER(10);
v_product_id NUMBER(10);
v_quantity NUMBER(10);
v_unit_price NUMBER(10, 2);
v_subtotal NUMBER(10, 2);
BEGIN
FOR i IN 1..20000 LOOP
    v_order_id := TRUNC(DBMS_RANDOM.VALUE(40000, 60000));
    v_product_id := TRUNC(DBMS_RANDOM.VALUE(20001, 39000));
    v_quantity := TRUNC(DBMS_RANDOM.VALUE(1, 10));
SELECT price INTO v_unit_price FROM products WHERE id = v_product_id;
    v_subtotal := v_unit_price * v_quantity;
  
INSERT INTO order_details(id, order_id, product_id, quantity, unit_price, subtotal)
VALUES(sales_seq.nextval, v_order_id, v_product_id, v_quantity, v_unit_price, v_subtotal);
  
IF MOD(i, 1000) = 0 THEN
COMMIT;
END IF;
END LOOP;
COMMIT;
END;
/
```
![](step1.3.png)

#### 第2步：设计权限及用户分配方案。至少两个用户。
```sql
-- 创建SALES_APP_USER用户并授权
CREATE USER sales_app_user IDENTIFIED BY 123456 DEFAULT TABLESPACE sales_data;
GRANT CREATE SESSION, CONNECT TO sales_app_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sales_app_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO sales_app_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON sales_orders TO sales_app_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON sales_order_details TO sales_app_user;

-- 创建SALES_ADMIN_USER用户并授权
CREATE USER sales_admin_user IDENTIFIED BY 123456 DEFAULT TABLESPACE sales_data;
GRANT CREATE SESSION, CONNECT TO sales_admin_user;
GRANT CREATE TABLESPACE, CREATE USER TO sales_admin_user;

```
#### 第三步：创建存储过程
```sql
-- 创建用户表插入数据的存储过程
CREATE  OR  REPLACE  PROCEDURE insert_user(
in_username IN  VARCHAR2,
in_password IN  VARCHAR2,
in_email IN  VARCHAR2,
in_phone IN  VARCHAR2
)
AS
BEGIN
 INSERT INTO users(id, username, password, email, phone)
 VALUES(users_seq.nextval, in_username, in_password, in_email, in_phone);
 COMMIT;
END;
/
-- 创建商品表插入数据的存储过程
CREATE  OR  REPLACE  PROCEDURE insert_product(
in_name IN  VARCHAR2,
in_price IN  NUMBER,
in_description IN  VARCHAR2,
in_image IN  VARCHAR2
)
AS
BEGIN
 INSERT INTO products(id, name, price, description, image)
 VALUES(products_seq.nextval, in_name, in_price, in_description, in_image);
 COMMIT;
END;
/
-- 创建订单表插入数据的存储过程
CREATE  OR  REPLACE  PROCEDURE insert_order(
in_user_id IN  NUMBER,
in_total_price IN  NUMBER,
in_create_time IN  TIMESTAMP
)
AS
BEGIN
 INSERT INTO orders(id, user_id, total_price, create_time)
 VALUES(orders_seq.nextval, in_user_id, in_total_price, in_create_time);
 COMMIT;
END;
/
-- 创建订单详情表插入数据的存储过程
CREATE  OR  REPLACE  PROCEDURE insert_order_detail(
in_order_id IN  NUMBER,
in_product_id IN  NUMBER,
in_quantity IN  NUMBER,
in_unit_price IN  NUMBER,
in_subtotal IN  NUMBER
)
AS
BEGIN
 INSERT INTO order_details(id, order_id, product_id, quantity, unit_price, subtotal)
 VALUES(order_details_seq.nextval, in_order_id, in_product_id, in_quantity, in_unit_price, in_subtotal);
 COMMIT;
END;
/
-- 创建查询所有商品信息的存储过程
CREATE  OR  REPLACE  PROCEDURE select_all_products
AS
BEGIN
 SELECT * FROM products;
END;
/
-- 根据商品名称查询相关商品信息的存储过程
CREATE  OR  REPLACE  PROCEDURE select_product_by_name(
in_name IN  VARCHAR2
)
AS
BEGIN
 SELECT * FROM products WHERE  name  LIKE  '%' || in_name || '%';
END;
/
-- 下单并生成订单的存储过程
CREATE  OR  REPLACE  PROCEDURE place_order(
in_user_id IN  NUMBER,
in_product_id IN  NUMBER,
in_quantity IN  NUMBER
)
AS
v_unit_price NUMBER(10, 2);
v_subtotal NUMBER(10, 2);
BEGIN
 SELECT price INTO v_unit_price FROM products WHERE id = in_product_id;
  v_subtotal := v_unit_price * in_quantity;
  
 INSERT INTO orders(id, user_id, total_price, create_time)
 VALUES(orders_seq.nextval, in_user_id, v_subtotal, SYSTIMESTAMP);
  
 INSERT INTO order_details(id, order_id, product_id, quantity, unit_price, subtotal)
 VALUES(order_details_seq.nextval, orders_seq.currval, in_product_id, in_quantity, v_unit_price, v_subtotal);
  
 COMMIT;
END;
/

-- 查询某个用户所有订单信息的存储过程
CREATE  OR  REPLACE  PROCEDURE select_orders_by_user_id(
in_user_id IN  NUMBER
)
AS
BEGIN
 SELECT o.id, o.total_price, o.create_time, od.product_id, od.quantity, od.unit_price, od.subtotal
 FROM orders o
 JOIN order_details od ON o.id = od.order_id
 WHERE o.user_id = in_user_id;
END;
/
```
#### 功能完善
```sql
-- 查询所有商品信息
DECLARE
 CURSOR c_products IS  SELECT * FROM products;
v_id NUMBER(10);
v_name VARCHAR2(100);
v_description VARCHAR2(255);
v_price NUMBER(10,2);
BEGIN
 OPEN c_products;
 LOOP
 FETCH c_products INTO v_id, v_name, v_description, v_price;
EXIT WHEN c_products%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE('Product ID: ' || v_id || ', Name: ' || v_name || ', Description: ' || v_description || ', Price: ' || v_price);
 END  LOOP;
 CLOSE c_products;
END;
/
-- 查询销售订单中的所有商品信息
DECLARE
 CURSOR c_sales_order_details IS
 SELECT sod.id, sod.order_id, p.id AS product_id, p.name AS product_name, sod.quantity, sod.unit_price, sod.subtotal
 FROM sales_order_details sod
 JOIN products p ON sod.product_id = p.id;
v_id NUMBER(10);
v_order_id NUMBER(10);
v_product_id NUMBER(10);
v_product_name VARCHAR2(100);
v_quantity NUMBER(10);
v_unit_price NUMBER(10,2);
v_subtotal NUMBER(10,2);
BEGIN
 OPEN c_sales_order_details;
 LOOP
 FETCH c_sales_order_details INTO v_id, v_order_id, v_product_id, v_product_name, v_quantity, v_unit_price, v_subtotal;
EXIT WHEN c_sales_order_details%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE('Order Detail ID: ' || v_id || ', Order ID: ' || v_order_id || ', Product ID: ' || v_product_id || ', Product Name: ' || v_product_name || ', Quantity: ' || v_quantity || ', Unit Price: ' || v_unit_price || ', Subtotal: ' || v_subtotal);
 END  LOOP;
 CLOSE c_sales_order_details;
END;
/

-- 查询某个客户的所有订单信息
DECLARE
v_customer_id NUMBER(10) := 1; -- 假设要查询客户ID为1的订单信息
 CURSOR c_sales_orders (p_customer_id NUMBER) IS
 SELECT *
 FROM sales_orders
 WHERE customer_id = p_customer_id;
v_id NUMBER(10);
v_order_date TIMESTAMP;
v_customer_id NUMBER(10);
BEGIN
 OPEN c_sales_orders(v_customer_id);
 LOOP
 FETCH c_sales_orders INTO v_id, v_order_date, v_customer_id;
EXIT WHEN c_sales_orders%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE('Order ID: ' || v_id || ', Order Date: ' || v_order_date || ', Customer ID: ' || v_customer_id);
 END  LOOP;
 CLOSE c_sales_orders;
END;
/

-- 查询某个商品的销售订单总金额
DECLARE
v_product_id NUMBER(10) := 1; -- 假设要查询商品ID为1的总销售额
v_total_amount NUMBER(10, 2);
BEGIN
 SELECT  SUM(sod.subtotal)
 INTO v_total_amount
 FROM sales_order_details sod
 WHERE sod.product_id = v_product_id;
  DBMS_OUTPUT.PUT_LINE('Total amount for product ID ' || v_product_id || ': ' || v_total_amount);
END;
/
-- 查询某个客户的购买金额总和
DECLARE
v_customer_id NUMBER(10) := 1; -- 假设要查询客户ID为1的总购买金额
v_total_amount NUMBER(10, 2);
BEGIN
 SELECT  SUM(sod.subtotal)
 INTO v_total_amount
 FROM sales_orders so
 JOIN sales_order_details sod ON so.id = sod.order_id
 WHERE so.customer_id = v_customer_id;
  DBMS_OUTPUT.PUT_LINE('Total amount for customer ID ' || v_customer_id || ': ' || v_total_amount);
END;
/
```
  
### 总结：
实验分析与总结：

这次学习让我更加升华了oracle的学习，对之前的实验进行了知识点的综合运用，也会基础知识有了更深的掌握。
  
总之，经过这次比较完整的项目，我学习到了Oracle数据库的常用知识以及数据库备份的相关知识，这在MySQL学习中没有涉及到，极大扩展了我的知识面，收获很大，也希望未来可以用这些知识去解决实际的问题。
  
通过设计和实施这个数据库备份方案，我们能够确保数据的安全性和可恢复性。

以下是一些总结与心得：
  
归档日志模式的使用：将数据库切换到归档日志模式是一种重要的安全措施。它确保了所有的日志记录都被保存下来，包括事务的细节和更改的历史。这样，在备份时，我们可以包含所有的数据更改，以便在需要时进行恢复操作。
  
数据恢复能力的保证：备份是为了能够在需要时进行数据恢复。通过定期备份整个数据库，我们可以确保在发生故障或数据丢失时，能够恢复到最近的可靠状态。备份文件的存储和管理要得当，以便在恢复操作时能够快速访问并恢复数据。
  
综上所述，一个完善的数据库备份方案是确保数据安全性和可恢复性的关键。通过合理的设置和策略，我们能够保护数据库免受数据丢失和故障的影响，并在需要时快速恢复数据，确保业务的连续性和可靠性。